import styled from "styled-components";

export const StyledPageGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(2, minmax(320px, 1fr));
  grid-template-rows: auto;
  gap: 24px;
  max-width: 1170px;
  padding-top: 40px;
  padding-bottom: 80px;

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    gap: 16px;
  }

  article {
    display: flex;
    flex-direction: column;
    justify-content: center;
    max-height: 500px;
    max-width: 500px;
    padding-left: 32px;

    @media screen and (max-width: 768px) {
      padding-left: 0;
    }
  }

  h1 {
    display: block;
    font-size: 64px;
    color: #03dac6;
    line-height: 0.9;
    margin: 0;
    padding: 0 0 8px 0;
    margin-bottom: 16px;
    color: transparent;
    background-image: linear-gradient(
      173.1deg,
      rgba(226, 66, 249, 0.94) 10.2%,
      rgba(79, 147, 249, 1) 77.3%
    );
    animation: rainbow 64s linear infinite;
    background-size: 60% 100%;
    -webkit-background-clip: text;
  }
  @keyframes rainbow {
    0% {
      background-position-x: 0;
    }
    to {
      background-position-x: 10240px;
    }
  }

  h2 {
    display: block;
    color: #404040;
    font-size: 46px;
    line-height: 1;
    margin: 0;
    padding: 0;
    margin-bottom: 16px;
  }

  h2 + p {
    display: inline-block;
    color: #404040;
    font-size: 16px;
    margin: 0;
    padding: 0;
  }
`;

export const StyledAuroraTitle = styled.div``;
