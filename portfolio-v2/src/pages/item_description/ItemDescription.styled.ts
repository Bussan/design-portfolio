import styled from "styled-components";

export const StyledPageGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: minmax(80px, auto);
  max-width: 1170px;
  padding-top: 40px;
  padding-bottom: 80px;

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
  }

  h1 {
    display: block;
    font-size: 55px;
    color: #03dac6;
    line-height: 0.9;
    margin: 0;
    padding: 0;
    margin-bottom: 16px;
    text-transform: uppercase;
  }

  h2 {
    display: block;
    color: #404040;
    font-size: 34px;
    font-weight: 400;
    line-height: 1;
    margin: 0;
    padding: 0;
    margin-bottom: 0;
    text-transform: uppercase;
  }

  .empty {
    content: "";
    display: block;
    grid-column: 1/7;
    grid-row: 3/4;
    height: 80px;
  }
`;

export const StyledItemTitle = styled.div`
  grid-column: 1/6;
  grid-row: 1/2;

  @media screen and (max-width: 768px) {
    grid-column: 1/2;
    grid-row: 1/2;
  }
`;

export const StyledItemSubtitle = styled.div`
  grid-column: 1/6;
  grid-row: 2/3;

  @media screen and (max-width: 768px) {
    grid-column: 1/2;
    grid-row: 2/3;
  }
`;

export const StyledItemDescription = styled.div`
  grid-column: 1/6;
  grid-row: 4/7;

  @media screen and (max-width: 768px) {
    grid-column: 1/2;
    grid-row: 4/5;
  }

  p {
    margin: 0;
  }
`;

export const StyledCompanyInfo = styled.div`
  grid-column: 7/9;
  grid-row: 3/7;

  @media screen and (max-width: 768px) {
    grid-column: 1/2;
    grid-row: 3/4;
  }

  ul {
    margin: 16px 0 0 0;
  }

  h4 {
    color: #ff0266;
    margin: 40px 0 0 0;
    text-transform: uppercase;

    &:first-of-type {
      margin: 0;
    }
  }
`;

export const StyledItemImages = styled.div`
  figure {
    margin: 4rem 0;
  }

  img {
    width: 100%;
  }
`;

export const StyledProblems = styled.div`
  color: #292e1e;
  grid-column: 1/9;
  grid-row: 7/8;
  grid-column-gap: 32px;
`;

export const StyledSolution = styled.div`
  color: #292e1e;
  grid-column: 1/9;
  grid-row: 8/9;
  grid-column-gap: 32px;
`;

export const StyledResults = styled.div`
  color: #292e1e;
  grid-column: 1/9;
  grid-row: 9/10;
  grid-column-gap: 32px;
`;

export const StyledSection4 = styled.div`
  color: #292e1e;
  grid-column: 1/9;
  grid-row: 10/11;
  grid-column-gap: 32px;
`;

export const StyledSection5 = styled.div`
  color: #292e1e;
  grid-column: 1/9;
  grid-row: 11/12;
`;

export const StyledContent = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 32px;

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    gap: 16px;
  }

  h2 {
    font-weight: 600;
  }
  p {
    margin: 0;
  }
`;
