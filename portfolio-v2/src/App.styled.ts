import styled from "styled-components";

export const StyledMainGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 150px 1fr 335px;
  grid-template-areas:
    "header"
    "main"
    "footer";
  grid-column-gap: 0px;
  grid-row-gap: 0px;
  width: 80vw;
  /* margin: 0 10vw; */
  max-width: 1170px;
`;

export const StyledNav = styled.div`
  grid-area: header;
`;

export const StyledMainContent = styled.div`
  grid-area: main;
  border-bottom: 2px solid #d9d9d9;
`;

export const StyledFooter = styled.div`
  grid-area: footer;
`;
