import React from "react";
import { StyledFooterOutter } from "./Footer.styled";

function Footer(): JSX.Element {
  return (
    <StyledFooterOutter>
      <div>
        <h4>Email</h4>
        <p>bpdemont.design@gmail.com</p>
      </div>

      <div>
        <h4>Social</h4>
        <ul>
          <li>Linkedin</li>
          <li>Facebook</li>
        </ul>
      </div>
      <div>
        <h4>Location</h4>
        <p>Yokohama, Japan</p>
      </div>
    </StyledFooterOutter>
  );
}
export default Footer;
