import styled from "styled-components";

export const StyledFooterOutter = styled.div`
  /* display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  grid-template-rows: auto;
  grid-column-gap: 16px;
  grid-row-gap: 16px; */
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  max-width: 1170px;
  height: 100%;
  padding: 50px 0;
  border-top: 2px solid #d9d9d9;

  @media screen and (max-width: 768px) {
    flex-direction: column;
  }

  > div {
    height: 8rem;
  }
  h4 {
    font-size: 12px;
    font-weight: bold;
    text-transform: uppercase;
  }

  p {
    font-weight: 400;
  }
`;
