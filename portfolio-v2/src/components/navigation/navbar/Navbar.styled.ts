import styled from "styled-components";

export const StyledNavOutter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: baseline;
  width: 80vw;
  max-width: 1170px;
  border-bottom: 2px solid #404040;
  padding-top: 48px;
  margin-bottom: 48px;

  h1 {
    font-size: 24px;
  }

  ul {
    display: flex;
    flex-direction: row;
    text-decoration: none;
    list-style: none;

    li {
      margin-left: 16px;
    }
  }
`;
