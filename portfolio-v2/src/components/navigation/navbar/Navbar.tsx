import React from "react";
import { Link, useMatch, useResolvedPath } from "react-router-dom";
import { StyledNavOutter } from "./Navbar.styled";

function Navbar(): JSX.Element {
  return (
    <StyledNavOutter>
      <ul>
        <CustomLink to="/">
          <h1>Bryan DeMont</h1>
        </CustomLink>
      </ul>

      <ul>
        <CustomLink to="/about">About</CustomLink>
        <CustomLink to="/contact">Contact</CustomLink>
      </ul>
    </StyledNavOutter>
  );
}
export default Navbar;

type CustomLink = {
  to: string;
  children: React.ReactNode;
};

function CustomLink({ to, children, ...props }: CustomLink) {
  const resolvedPath = useResolvedPath(to);
  const isActive = useMatch({ path: resolvedPath.pathname, end: true });
  return (
    <li className={isActive ? "active" : ""}>
      <Link to={to} {...props}>
        {children}
      </Link>
    </li>
  );
}
