import PropTypes from "../props/PropTypes";

import DashBoardImg from "../../assets/dashboard.png";

// export const genBlogId = (): string => Math.random().toString(36).slice(-6);

export const WorksData: PropTypes[] = [
  {
    pageId: "hanabi-glass",

    cardTitle: "Hanabi Glass",

    cardSubtitle: "Luxury Glass Pens",

    cardImg:
      "https://images.unsplash.com/flagged/photo-1551373916-bdaddbf4f881?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",

    title: "Hanabi Glass",

    subtitle: "Logo creation facilitating change in brand identiy",

    companyName: "Hanabi Glass Studio",

    projectType: "Logo Design",

    projectTypeAdd: "Brand Strategy",

    projectDate: "2016",

    projectTools1: "Adobe Illustrator",

    projectTools2: "Pen & Paper",

    projectTools3: "",

    projectTools4: "",

    projectTools5: "",

    body: "During the initial meeting to discuss the creation of the logo I asked the owner what he wanted his shop to become and what it is that he would like to make. We discussed some of the common things going on in the lamp working world as well as some of the things that people often do. Then I asked questions so I could better understand how lamp working differs from glass blowing. Once I had a better idea of how they differ I began to brainstorm about the type of business that he would like Hanabi Glass Studio to become as well as the type of customers he was interested in. While the owner is very skilled and can make almost anything given the right tools, find a niche for his skills would take time to discuss. Over the following weeks through in person discussions and texts, the owner and I agreed that creating a shop that creates luxury glass jewelry would be the direction he would take the company. With an understanding of the company and the brand that the owner wanted, I was no able to finalize the last part of the logo designs.",

    section1Header: "Empathy (better term later)",

    section1Body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis consequatur inventore laborum, rem quas voluptatem obcaecati nobis, eos omnis dicta cum quos assumenda labore fugit cumqueillum beatae corrupti repellendus!",

    section2Header: "Define (better term later)",

    section2Body:
      "To start off with I met with the owner to discuss what his goals were for his glass shop. Understanding what he wants the shop be and or represent heavily influences what I plan to design as well as how the logo will be used to help grow his brand in the direction he desires. During the discussion I learned that he wants to make glass items that wouldn't look out of place in a store that caters to clients in the luxury genre. Along with the information that currently most of his sales go through private messages on Instagram, I had a general idea of what I was going to do.",

    section3Header: "Ideate (better term later) - pics of notebook",

    section3Body:
      "During the Ideate phase of my process I spent time researching not only other glass shops but also luxury items sold by other companies that would be comparable to glass items, jewlery, and artwork. From this I learnd that a common item made, collected, and sold of high prices is marbles. In this industry marbles are usually a symbol of each artists style and is a way to also display their skills.Adding this to the fact that Instagrams profile photos are circular, the idea of a marble as the logo was solidfied. As the shop is called 'Hanabi Glass' (firewor glass) I spoke with the client and he agreed that a firework design on a marble similar to a Japanese firework motif foun on Yukata during fireworks festivals would be the goal for the logo's final design",

    section4Header: "Prototype (better term later)",

    section4Body:
      "While I had an idea of the direction and the goal of the logo, the next part mean I had to create a near final version of the logo in illustrator. To further simplify the design I reduced the amount of details and kept the most defining features of the logo to the point where it was still easily recognizable.",

    section5Header: "Test (better term later)",

    section5Body:
      "Before finalizing the design I tested the logo in various use cases. The logo was tested inverted, small, big, on business cards, clothing, packaging, and on different colored backgrounds. Once the logo was useable in each of these different ways while being easily recognizable, the final design was submitted to teh client and soon after approved. The final files were created and deliverd to the client so that they can easily use the logo on social media, for print purposes, or to send to other shops for clothing and or packaging creating purposes.",

    img1: "https://live.staticflickr.com/65535/53232058537_12371be199_k.jpg",
    img2: "",
    img3: "",
    img4: "",
    img5: "",
  },
  {
    pageId: "realestate",

    cardTitle: "Real Estate",

    cardSubtitle: "Website Redesign",

    cardImg:
      "https://images.unsplash.com/flagged/photo-1551373916-bdaddbf4f881?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",

    title: "PLAZA HOMES",

    subtitle: "Website Redesign and Frontend development",

    companyName: "PLAZA HOMES",

    projectType: "Web design",

    projectTypeAdd: "Frontend Development",

    projectDate: "2018",

    projectTools1: "Adobe Photoshop",

    projectTools2: "Adobe Illustrator",

    projectTools3: "Visual Studio Code",

    projectTools4: "Codekit",

    projectTools5: "Pug, SCSS",

    body: "Julius inc., a car shop that I frequent whose owner I have enjoyed talking to many times. I was asked to make a business card that would represent the shop and shout race car the moment you looked at it. After serveral ideas and revisions, this is the layout and colors that were decided upon. The cards were printed from a shop online in America using Spot UV and rounded corners.",

    section1Header: "The Chanllenge",

    section1Body:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis consequatur inventore laborum, rem quas voluptatem obcaecati nobis, eos omnis dicta cum quos assumenda labore fugit cumqueillum beatae corrupti repellendus!",

    section2Header: "The Results",

    section2Body: "The Results",

    section3Header: "The Solution",

    section3Body: "The Results",

    section4Header: "The Chanllenge",

    section4Body: "The Results",

    section5Header: "The Chanllenge",

    section5Body: "The Results",

    img1: "https://live.staticflickr.com/65535/53232058537_12371be199_k.jpg",
    img2: "https://live.staticflickr.com/65535/53232058537_12371be199_k.jpg",
    img3: "https://live.staticflickr.com/65535/53232058537_12371be199_k.jpg",
    img4: "https://live.staticflickr.com/65535/53232058537_12371be199_k.jpg",
    img5: "https://live.staticflickr.com/65535/53232058537_12371be199_k.jpg",
  },
  {
    pageId: "dashboard",

    cardTitle: "Dashboard Redesign",

    cardSubtitle: "Internal Dashboard Redesign",

    cardImg:
      "https://images.unsplash.com/flagged/photo-1551373916-bdaddbf4f881?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",

    title: "Dashboard",

    subtitle: "Aggregrated Data Dashboard",

    companyName: "Undisclosed",

    projectType: "Design",

    projectTypeAdd: "UX/UI",

    projectDate: "2022 - 2023",

    projectTools1: "Figma",

    projectTools2: "Adobe Illustrator",

    projectTools3: "VS Code",

    projectTools4: "GitLab",

    projectTools5: "Design Thinking Framework",

    body: "The goal of this project was to re-design an internal tools dashboard and improve the user experience. Some of the main problems the client was looking to solve were; inconsistent design, steep learning curve, reduce help desk requests, increase number of users, and reduce the time needed to train users to properly use the tool. The biggest challenge was understanding the tool on a deeper level so that I could help rebuild and modify user flows as well as create a more intuitive user experience along with a consistent and visually pleasing user interface.",

    section1Header: "Diagnostic Analysis",

    section1Body:
      "Conducted a comprehensive analysis of the original dashboard website, focusing on usability, consistency, and accessibility. Broke down the UI into parts and categorized them based on their functions and locations. Identified issues with poor organization, lack of consistency, small fonts, and poor accessibility.",

    section2Header: "Stakeholder Collaboration",

    section2Body:
      "Collaborated closely with the project team, app developers, and employees who rely on the tool to understand their goals and objectives. Gathered feedback from stakeholders on pain points and areas for improvement. Through extensive communication I was able to gain a deeper understading of the product as well as the thinking and goals behind it's original creation.",

    section3Header: "Iterative Redesign Process",

    section3Body:
      "Redesigned the existing dashboard to prioritize accessibility and consistency, addressing font sizes, contrast ratios, and layout organization. Implemented new features such as advanced search functionality to improve user experience and efficiency. Introduced features like the ability to hide columns and organize data tables for better data visualization and user interaction.",

    section4Header: "Content Migration and Optimization",

    section4Body:
      "Managed content migration through version control, ensuring a seamless transition to the redesigned dashboard. Implemented changes gradually after thorough review in development environments, maintaining data integrity and accuracy. No changes were made to the content strategy, focusing instead on improving usability and functionality.",

    section5Header: "Launch and Continuous Improvement",

    section5Body:
      "Oversaw the launch of the redesigned dashboard, ensuring a smooth transition for users. Monitored user feedback and analytics post-launch to assess the impact of the redesign. Observed a significant improvement in the ease of use of the dashboard, resulting in a 71% decrease in tech support calls for assistance. Used feedback to iterate and improve other necessary user flows in turn increasing user number and lowering help desk inquiries.",

    img1: DashBoardImg,
    img2: "",
    img3: "",
    img4: "",
    img5: "",
  },
];
