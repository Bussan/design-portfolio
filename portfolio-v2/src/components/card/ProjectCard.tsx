import React from "react";
import { StyledProjectCard } from "./ProjectCard.styled";
import { Link } from "react-router-dom";

//add prop types here
interface Props {
  src?: string;
  title?: string;
  subtitle?: string;
  pageId?: string;
}

//Add : Props after the array to bind the prop types
function ProjectCard({ src, title, subtitle, pageId }: Props): JSX.Element {
  console.log("from project card", pageId);
  return (
    <StyledProjectCard>
      <img src={src} />

      <figcaption>
        <h3>{title}</h3>
        <p>{subtitle}</p>
      </figcaption>

      <Link to={`/${pageId}`}></Link>
    </StyledProjectCard>
  );
}
export default ProjectCard;
